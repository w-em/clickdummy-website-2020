;(function ($) {
  'use strict';

  $.plugin('wemYoutube', {

    defaults: {
      selector: '*[data-youtube]',
      image: '*[data-youtube-image]',
    },

    init: function () {
      var me = this;
      // me.$element = $(me.opts.selector);
      me.youtubeUrl = me.$el.attr('data-youtube');
      me.imageUrl = me.$el.attr('data-youtube-image');
      me.$btn = me.$el.find('.btn');
      me.applyDataAttributes();
      me.registerEvents();
      var imageUrl = me.getYoutubePreviewImage();

      me.$previewImage = $('<img src="'+imageUrl+'" class="img-fluid w-100">');
      me.$previewImage.on('click', function(e) {
        me.insertIframe();
        me.hidePreviewImage();
      });
      me.$btn.on('click', function(e) {
        me.insertIframe();
        me.hidePreviewImage();
      });
      me.$el.append(me.$previewImage);
    },

    insertIframe: function () {
      var me = this;
      me.$iframe = $('<iframe frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen"></iframe>');
	  me.$iframe.css('z-index', 20);
      me.$iframeWrapper = $('<div class="video-container"></div>')
      me.$iframe.attr('src', me.youtubeUrl + '?autoplay=1');
      me.$iframeWrapper.append(me.$iframe);
      me.$el.append(me.$iframeWrapper);
    },

    hidePreviewImage: function () {
      var me = this;
      me.$previewImage.hide()
      me.$btn.hide()
    },

    getYoutubePreviewImage: function () {
      if (this.imageUrl) {
        var previewUrl = this.imageUrl;
      }
      else {
        var regex = /youtube\.com.*(\?v=|\/embed\/)(.{11})/;
        var videoId = this.youtubeUrl.match(regex).pop();
        var previewUrl = '//img.youtube.com/vi/' + videoId + '/0.jpg';
      }
      return previewUrl;
      // var youtube_video_id = iframe_src.match(/youtube\.com.*(\?v=|\/embed\/)(.{11})/).pop();
    },

    /**
     * Registers all necessary event listeners.
     *
     * @public
     * @method registerEvents
     */
    registerEvents: function () {

    },

    onResize: function () {

    },

    destroy: function () {
      this._destroy();
    }
  });
}(jQuery));
