(function ($, window) {

	// svg for everybody
	svg4everybody();

	window.StateManager.init([
		{
			state: 'xs',
			enter: 0,
			exit: 359
		},
		{
			state: 'sm',
			enter: 360,
			exit: 575
		},
		{
			state: 'hd',
			enter: 576,
			exit: 767
		},
		{
			state: 'md',
			enter: 768,
			exit: 991
		},
		{
			state: 'lg',
			enter: 992,
			exit: 1199
		},
		{
			state: 'xl',
			enter: 1200,
			exit: 1679
		},
		{
			state: 'xxl',
			enter: 1680,
			exit: 9999999
		}
	]);

	var breakpoints = StateManager.getBreakpoints();

	window.StateManager.addPlugin('[data-slider]', 'wemSlider');
	window.StateManager.addPlugin('[data-youtube]', 'wemYoutube');

	// anchors/scroll-to
	var section_anchors = $('.anchor-scroll > a');
	section_anchors.on('click', function (e) {
		e.preventDefault();

		var section = $($(this).attr('href'));

		if (section.length > 0) {
			$('html, body').animate({
				scrollTop: section.offset().top
			}, 750);

			section_anchors.removeClass('section-anchor--active');
			$(this).addClass('section-anchor--active');
		}
	});

	// mobile menu
	const menu = new MmenuLight(
		document.querySelector("#mobile-menu")
	);

	const drawer = menu.offcanvas({position: 'right'});
	const navigation = menu.navigation({});

	document.querySelector('.mobile-menu-btn').addEventListener('click', (evnt) => {
		evnt.preventDefault();
		drawer.open();
	});

	var path = window.location.pathname;
	var page = path.split("/").pop();
	page = page.split(".").shift();
	page = page.split("-").shift();

	page = (page == 'interview' || page == 'arbeiten' || page == 'team' || page == 'jobs') ? 'agency' : page;

	if (page.length > 0 && typeof page == 'string') {
		$('.navigation__menu').find('a[href*="' + page + '"]').addClass('font-weight-bold');
	}


})(jQuery, window);
