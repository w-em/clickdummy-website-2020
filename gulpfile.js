const gulp = require('gulp');
const gulpIf = require('gulp-if');
const browserSync = require('browser-sync').create();
const sass = require('gulp-sass');
const cssmin = require('gulp-cssmin');
const uglify = require('gulp-uglify');
const imagemin = require('gulp-imagemin');
const concat = require('gulp-concat');
const jsImport = require('gulp-js-import');
const sourcemaps = require('gulp-sourcemaps');
const clean = require('gulp-clean');
const isProd = process.env.NODE_ENV === 'prod';
const gulpTwig = require('gulp-twig');
const beautify = require('gulp-jsbeautifier');
const svgSymbols = require('gulp-svg-symbols');

function twig() {
	return gulp.src([
		'src/*.twig',
		'src/pages/**/*.twig'
	]).pipe(gulpTwig())
		.pipe(beautify({
			indent_size: 2
		}))
		.pipe(gulp.dest('docs'))
};

function css() {
	return gulp.src('src/sass/style.scss')
		.pipe(gulpIf(!isProd, sourcemaps.init()))
		.pipe(sass({
			includePaths: ['node_modules']
		}).on('error', sass.logError))
		.pipe(gulpIf(!isProd, sourcemaps.write()))
		.pipe(gulpIf(isProd, cssmin()))
		.pipe(gulp.dest('docs/css/'));
}

function js() {
	return gulp.src(['src/js/vendors/*.js', 'src/js/base/*.js', 'src/components/**/*.js', 'src/js/index*.js'])
		.pipe(jsImport({
			hideConsole: true
		}))
		.pipe(concat('all.js'))
		.pipe(gulpIf(isProd, uglify()))
		.pipe(gulp.dest('docs/js'));
}

function img () {
	return gulp.src([
		'src/assets/img/*',
		'src/assets/img/**/*'
	]).pipe(gulpIf(isProd, imagemin()))
		.pipe(gulp.dest('docs/img/'));
}

function vid () {
	return gulp.src([
		'src/assets/video/*'
	]).pipe(gulp.dest('docs/video/'));
}

function fonts () {
	return gulp.src([
		'src/assets/fonts/*',
		'src/assets/fonts/**/*'
	]).pipe(gulpIf(isProd, imagemin()))
		.pipe(gulp.dest('docs/assets/fonts/'));
}

function serve() {
	browserSync.init({
		open: true,
		server: './docs'
	});
}

function browserSyncReload(done) {
	browserSync.reload();
	done();
}

function svgSprite() {
	return gulp.src([
		'src/assets/svg/*.svg',
		'src/assets/svg/**/*.svg'
	]).pipe(svgSymbols({
		class: 'icon--%f',
		fontSize: 32,
		title: '%f',
		svgAttrs: {
			class: ``,
			'aria-hidden': `true`,
			style: ``
		},
		templates: [`default-svg`]
	})).pipe(gulp.dest("docs/img"))
}

function watchFiles() {
	gulp.watch('src/assets/**/*.svg', gulp.series(svgSprite, browserSyncReload));
	gulp.watch('src/**/*.twig', gulp.series(twig, browserSyncReload));
	gulp.watch('src/**/*.scss', gulp.series(css, browserSyncReload));
	gulp.watch('src/**/*.js', gulp.series(js, browserSyncReload));
	gulp.watch('src/components/**/*.js', gulp.series(js, browserSyncReload));
	gulp.watch('src/assets/img/**/*.*', gulp.series(img, browserSyncReload));
	gulp.watch('src/assets/video/**/*.*', gulp.series(vid, browserSyncReload));

	return;
}

function del() {
	return gulp.src('docs/*', {read: false})
		.pipe(clean());
}

exports.css = css;
exports.twig = twig;
exports.fonts = fonts;
exports.js = js;
exports.del = del;
exports.svgSprite = svgSprite;
exports.serve = gulp.parallel(svgSprite, fonts, twig, css, js, img, vid, watchFiles, serve);
exports.default = gulp.series(del, svgSprite, fonts, twig, css, js, img, vid);